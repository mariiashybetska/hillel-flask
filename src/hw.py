from flask import Flask, Response, request
from faker import Faker

import os
import random

from utils import get_avg_metrics, parse_length, get_rate, gen_password, gen_password_with_eng_chars, format_list, \
    get_customer_by_name, safe_get_customer_by_name
from db import exec_query


app = Flask('app')

@app.route('/')
def title():
    return 'hello'


'''
ПЕРВАЯ ДОМАШКА:
1) Функция возвращает содержимое файла с установленными пакетами в текущем проекте (requirements.txt)
2) Выводит 100 случайно сгенерированных юзеров (почта + имя).
3) Вернуть средний рост и средний вес (в см и кг соответственно) для студентов из файла hw.csv
'''


@app.route('/requirements')
def get_requirements():
    req_pass = os.path.join(os.getcwd(), 'requirements.txt')
    with open(req_pass) as r:
        return r.read()


@app.route('/fake_names')
def get_fake_names():
    fake = Faker()
    return '<br>'.join (
        f'name: {fake.name()}, email: {fake.email()}' for i in range(100)
    )


@app.route('/avg_metrics')
def avg_metrics():
    return get_avg_metrics()


'''
ВТОРАЯ ДОМАШКА:
1) Добавить в view-функцию генерации пароля необязательный параметр, который регулирет включение символов 
англ алфавита в сгенерированный пароль. Работает как булевый параметр: 1 -> True, 0 -> False. 
По умолчанию символы не включаются.
2) Добавить во view-функцию получение курса биткойна возможность выбора валюты. 
Параметр необязательный, по умолчанию используется USD+
3) Создать view-функцию подсчета уникальных имен (FirstName) в таблице Customers.+
4) Создать view-функцию, которая выводит количество записей из таблицы Tracks+
5) Вывести оборот компании из таблицы Invoice_Items как сумму всех покупок (UnitPrice * Quantity)+
'''


@app.route('/password')
def gen_random_password():
    eng_chars = request.args.get('eng_chars', 0)
    try:
        length = parse_length(request, 10)
        if eng_chars == '1':
            return gen_password_with_eng_chars(length)
        else:
            return gen_password(length)
    except Exception as ex:
        return f'{ex}'



@app.route('/bitcoin')
def get_bitcoin_rate():
    currency = request.args.get('currency', 'USD')
    return get_rate(currency)


@app.route('/unique-names')
def get_unique_names():
    query = 'SELECT COUNT(distinct FirstName) FROM customers;'
    result = exec_query(query)[0][0]
    return f'There are {result} unique first names in table Customers'


@app.route('/tracks_count')
def get_tracks_count():
    query = 'SELECT COUNT(distinct TrackId) FROM Tracks;'
    result = exec_query(query)[0][0]
    return f'There are {result} records in table Tracks'


@app.route('/gross_turnover')
def get_gross_turnover():
    query = 'SELECT SUM(UnitPrice * Quantity) FROM invoice_items;'
    result = round(exec_query(query)[0][0], 2)
    return f'Total profit is {result}'


'''
Получение списка клиентов с возможностью фильтрации по имени и фамилии.
/customers - с возможностью sql-иньекции
/safe_customers - с защитой от иньекций
'''

@app.route('/customers')
def get_customers():
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')

    record = get_customer_by_name(first_name, last_name)

    return format_list(record)



@app.route('/safe_customers')
def safe_get_customers():
    first_name = request.args.get('first_name')
    last_name = request.args.get('last_name')

    record = safe_get_customer_by_name(first_name, last_name)

    return format_list(record)


if __name__ == '__main__':
    app.run(port=5001, debug=True)



