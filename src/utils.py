import os
import string
import requests
import json
import random

from db import exec_query, exec_query_with_args


def get_avg_metrics():
    hw_pass = os.path.join(os.getcwd(), 'hw.csv')
    INCH_TO_CM = 2.54
    POUNDS_TO_KG = 0.454

    with open(hw_pass) as f:
        content = f.read()
        content = content.split('\n')[1::]
        number_of_records = 0
        sum_of_h = 0
        sum_of_w = 0

        for row in content:
            if not row:
                continue
            number_of_records += 1
            h = float(row.split(',')[1])
            w = float(row.split(',')[2])
            sum_of_h += h
            sum_of_w += w

        avg_h = round(sum_of_h / number_of_records * INCH_TO_CM, 2)
        avg_w = round(sum_of_w / number_of_records * POUNDS_TO_KG, 2)
    return f'Average height: {avg_h} and average weight {avg_w}'



def get_rate(currency):
    url = 'https://bitpay.com/api/rates'
    response = requests.get(url)
    rates = json.loads(response.text)

    for rate in rates:
        if rate['code'] == currency:
            return f'1 Bitcoin = {rate["rate"]} {currency}'
    return f'Currency {currency} not found!'


def parse_length(request, default=10):
    length = request.args.get('length', str(default))
    if not length.isnumeric():
        raise ValueError('VALUE ERROR: enter integer!')
    length = int(length)
    if not 3 <= length <= 10:
        raise ValueError('RANGE ERROR: enter integer between 3 and 10')
    return length


def gen_password(number_of_ints):
    return ''.join([str(random.randint(0, 9)) for _ in range(number_of_ints)])


def gen_password_with_eng_chars(length):
    number_of_eng_chars = 2
    number_of_ints = length - number_of_eng_chars
    password = ''.join([str(random.randint(0, 9)) for _ in range(number_of_ints)]) + \
                ''.join([str(random.choice(string.ascii_letters)) for _ in range(number_of_eng_chars)])
    return password


def get_customer_by_name(first_name, last_name):
    # получаем данные с помлщью динамически формируемого sql-запроса через форматную строку
    query = 'SELECT FirstName, LastName FROM customers'
    filters = []
    if first_name or last_name:
        query += ' WHERE'

    if first_name:
        filters.append(f' FirstName = "{first_name}"')

    if last_name:
        filters.append(f' LastName = "{last_name}"')

    if filters:
        query += ' AND '.join(filters)

    records = exec_query(query)
    return records


def safe_get_customer_by_name(first_name, last_name):
    # получаем данные с помощью параметров запроса, которые обрабатывает сама БД

    query = 'SELECT FirstName, LastName FROM customers'
    filters = []
    params = []  # кол-во элементов списка params должно равняться кол-ву ? в запросе

    if first_name or last_name:
        query += ' WHERE'

    if first_name:
        filters.append(f' FirstName=?')
        params.append(first_name)

    if last_name:
        filters.append(f' LastName=?')
        params.append(last_name)

    if filters:
        query += ' AND '.join(filters)

    records = exec_query_with_args(query, params)

    '''
    мне данный метод запроса напоминает форматирование строк:
     'Hello, {}!'.format('Vasya') - где мы в скобках после строки передаем переменные
    '''
    return records


def format_list(lst):
    return '<br>'.join(str(elem) for elem in lst)

