import os
import sqlite3


def exec_query(query):
    # выполнение запроса
    db_pass = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_pass)
    cur = conn.cursor()
    cur.execute(query)
    records = cur.fetchall()
    return records


def exec_query_with_args(query, args):
    # выполнение запроса с параметрами, которые обрабатывает сама БД
    db_pass = os.path.join(os.getcwd(), 'chinook.db')
    conn = sqlite3.connect(db_pass)
    cur = conn.cursor()
    cur.execute(query, args)
    records = cur.fetchall()
    return records


'''
Примеры sql-иньекций:
"UNION ALL SELECT Name, Composer FROM tracks --
"UNION ALL SELECT tbl_name, type FROM sqlite_master --

Необходимо подставить после указанного значения get-параметра:
http://127.0.0.1:5001/safe_customers?first_name=Mark"UNION ALL SELECT Name, Composer FROM tracks --
'''