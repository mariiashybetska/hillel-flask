--DDL (data definition LANGUAGE) язык определения данных--
CREATE table/index
ALTER 
DROP

--DML (data manipulation language) язык манипулирования данными--
INSERT into 
UPDATE 
DELETE

--DQL (data query language) язык запросов--
SELECT


--DCL (data control language) - язык контроля данных--
GRANT
REVOKE


SELECT city
      ,count(FIO) AS cnt_students
  FROM students
  WHERE age BETWEEN 20 AND 25
  GROUP BY city
  HAVING count(FIO) >=3
